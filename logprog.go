package logprog

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// логирование в файла и стардартный os.stdout
type LogFname struct {
	Fname      string
	fhandler   *os.File
	fileisopen bool
}

func (f *LogFname) openlogfile() error {
	if f.fileisopen == false && f.fhandler == nil {
		fh, err := os.OpenFile(f.Fname, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		f.fhandler = fh
		f.fileisopen = true
	}
	return nil
}
func (f LogFname) Write(p []byte) (n int, err error) {
	if f.fileisopen == false {
		if err := f.openlogfile(); err != nil {
			panic(err)
		}
	}
	pc, file, line, ok := runtime.Caller(4)
	if !ok {
		file = "?"
		line = 0
	}

	fn := runtime.FuncForPC(pc)
	var fnName string
	if fn == nil {
		fnName = "?()"
	} else {
		dotName := filepath.Ext(fn.Name())
		fnName = strings.TrimLeft(dotName, ".") + "()"
	}

	log.Printf("%s:%d %s: %s", filepath.Base(file), line, fnName, p)
	_, err = f.fhandler.Write([]byte(fmt.Sprintf("%s:%d %s: %s", filepath.Base(file), line, fnName, p)))
	if err != nil {
		return 0, err
	}
	return len(p), nil
}
